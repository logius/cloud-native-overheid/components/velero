# Overall  functionality

## Install velero operator

The velero role installs the Velero Operator in the cluster.

## Additional functions

### _Create backup schedule_

Creates a backup schedule on every 4 hour.

``` bash
ansible-playbook /playbook/run-role.yml -e galaxy_url=https://gitlab.com/logius/cloud-native-overheid/components/velero.git \
    -e @<config file> \
    -t configurebackup \
    -e backup_namespace=<namespace name> \
    [-e backup_ttl=<99h99m99s>] \
    [-e "backup_schedule='<schedule>'"]
```

`backup_ttl` is an optional parameter in the form of a period with only `h`, `m` and/or `s` parameters. Defaults to 1 week.

`backup_schedule` is an optional parameter in the form of a cron expression or an expression as in the Velero documentation. Defaults to `@every 4h`. Note the double and single quotes: required when the value contains spaces, as a cron expression usually does.

### _Create a temp backup/restore snapshot_

Use case: create a recovery snapshot in a upgrade scenario.

``` bash
ansible-playbook /playbook/run-role.yml -e galaxy_url=https://gitlab.com/logius/cloud-native-overheid/components/velero.git \
    -e @<config file> \
    -t create_ns_snapshot \
    -e backup_namespace=<namespace name> \
    -e snapshot_name=<snapshot name> \
    [-e backup_ttl=<99h99m99s>]
...
other code
...

...
when failed, restore the complete namespace
...
ansible-playbook /playbook/run-role.yml -e galaxy_url=https://gitlab.com/logius/cloud-native-overheid/components/velero.git \
    -e @<config file> \
    -t restore_ns_snapshot \
    -e backup_namespace=<namespace name> \
    -e snapshot_name=<snapshot name>
```

`backup_ttl` is an optional parameter in the form of a period with only `h`, `m` and/or `s` parameters. Defaults to 1 hour.
